package io.anuke.mindustry.type;

import com.badlogic.gdx.utils.Array;

public class Category {
    public static Category
            weapon = new Category("weapon"),
            production = new Category("production"),
            distribution = new Category("distribution"),
            liquid = new Category("liquid"),
            power = new Category("power"),
            defense = new Category("defense"),
            crafting = new Category("crafting"),
            units = new Category("units");

    public static Array<Category> categories = Array.with(
            weapon, production, distribution, liquid, power, defense, crafting, units);

    public String name;

    public Category(String name) {
        this.name = name;
    }
}
