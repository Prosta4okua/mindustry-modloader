package io.anuke.mindustry;

import io.anuke.mindustry.core.*;
import io.anuke.mindustry.io.BundleLoader;
import io.anuke.mindustry.modloader.utils.TexturePackHandler;
import io.anuke.ucore.core.Settings;
import io.anuke.ucore.core.Timers;
import io.anuke.ucore.modules.ModuleCore;
import io.anuke.ucore.util.Log;

import static io.anuke.mindustry.Vars.*;

public class Mindustry extends ModuleCore{

    @Override
    public void init(){
        Timers.mark();

        Vars.init();

        debug = Platform.instance.isDebug();

		Log.setUseColors(false);

        Settings.load(headless ? "io.anuke.mindustry.server" : "io.anuke.mindustry");

		module(new Modloader());

        TexturePackHandler.registerTexturePacks();

		BundleLoader.load();
		ContentLoader.load();

		Vars.modloader.modInit();

		module(logic = new Logic());
		module(world = new World());
		module(control = new Control());
		module(renderer = new Renderer());
		module(ui = new UI());
		module(netServer = new NetServer());
		module(netClient = new NetClient());

		Vars.modloader.modPostInit();




    }

    @Override
    public void postInit(){
        Log.info("Time to load [total]: {0}", Timers.elapsed());
    }

    @Override
    public void render(){
        threads.handleBeginRender();
        super.render();
        threads.handleEndRender();
    }

}
