package io.anuke.mindustry.core;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Base64Coder;
import com.esotericsoftware.yamlbeans.YamlReader;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.modloader.*;
import io.anuke.mindustry.modloader.api.Hooks;
import io.anuke.mindustry.modloader.objects.Mod;
import io.anuke.mindustry.modloader.objects.ModContainer;
import io.anuke.mindustry.modloader.objects.ModInfo;
import io.anuke.mindustry.modloader.utils.Atlas;
import io.anuke.mindustry.modloader.utils.Logger;
import io.anuke.mindustry.modloader.utils.Utils;
import io.anuke.ucore.core.Settings;
import io.anuke.ucore.graphics.Draw;
import io.anuke.ucore.modules.Module;
import io.anuke.ucore.util.Log;
import org.reflections.Reflections;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Modloader extends Module {

    public Array<String> enabledMods;
    //public Atlas atlas;

    private ModContainer modContainer = new ModContainer();
    private Set<Class<?>> modClasses;
    private static final Class[] parameters = new Class[]{URL.class};
    private Array<String> dependencies = new Array<>();

    public Modloader() {
        enabledMods = Settings.getJson("enabledMods", Array.class);

        Vars.modloader = this;

        loadMods();
    }

    public static void addURL(URL u) throws IOException {
        URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        Class sysclass = URLClassLoader.class;

        try {
            Method method = sysclass.getDeclaredMethod("addURL", parameters);
            method.setAccessible(true);
            method.invoke(sysloader, new Object[]{u});
        } catch (Throwable t) {
            t.printStackTrace();
            throw new IOException("Error, could not add URL to system classloader");
        }
    }

    private void loadMods() {
        Hooks.initializeRenderHookMap();

        if (!Vars.headless) {
            Path atlasPath = Paths.get(Vars.modDirectory + "/tmp/atlas");

            if (Files.exists(atlasPath)) {
                if (atlasPath.toFile().isDirectory()) {
                    for (File file : Objects.requireNonNull(atlasPath.toFile().listFiles())) {
                        if (!file.delete())
                            Logger.err("Failed to delete file: {0}", file.getName());
                    }
                } else {
                    if (atlasPath.toFile().delete())
                        Logger.warn("Deleted {0}", Vars.modDirectory + "/tmp/atlas");
                }
            }
        }
        
        for (FileHandle fileHandle : Vars.modDirectory.list()) {
            //Skip files that can't be mods or libraries.
            if (!fileHandle.extension().equals("jar"))
                if(!fileHandle.isDirectory())
                    if(!fileHandle.extension().equals("zip"))
                        continue;
            Log.info("Loading mod {0}", fileHandle.nameWithoutExtension());
            File modFile = fileHandle.file();
            Mod mod = new Mod();
            mod.file = modFile;

            boolean library = false;
            if (!fileHandle.isDirectory()) {
                try {
                    ZipFile zf = new ZipFile(mod.file);
                    ZipEntry ze = zf.getEntry("modmeta");
                    
                    if (ze != null) {
                        InputStream input = zf.getInputStream(ze);

                        YamlReader yr = new YamlReader(new InputStreamReader(input, StandardCharsets.UTF_8));
                        mod.modInfo = yr.read(ModInfo.class);

                        if (mod.modInfo.id == null) mod.modInfo.id = mod.modInfo.name;
                        mod.uuid = Base64Coder.encodeString(mod.modInfo.author + mod.modInfo.name);
                    } else {
                        Logger.warn("File {0} missing modmeta, presuming library", fileHandle.nameWithoutExtension());
                        library = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            } else if (fileHandle.child("modmeta").exists()) {
                try {
                    YamlReader yr = new YamlReader(fileHandle.child("modmeta").reader());
                    mod.modInfo = yr.read(ModInfo.class);

                    mod.uuid = Base64Coder.encodeString(mod.modInfo.author + mod.modInfo.name);
                } catch (Exception e) {
                    System.out.println("aeeeeeeeeeeeeeeeeeeeee");
                    e.printStackTrace();
                }
            } else {
                Logger.info("Folder found that does not have modmeta file");
                continue;
            }

            if (library) {
                try {
                    addURL(modFile.toURI().toURL());
                    continue;
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            }

            //Duplication check
            boolean duplicate = false;
            for (Mod m : modContainer.getMods()) {
                if (m.uuid.equals(mod.uuid)) {
                    Logger.err("Duplicate mod, not loading");
                    duplicate = true;
                    break;
                }
            }
            if (duplicate) continue;

            if (!enabledMods.contains(mod.uuid, false) || (Vars.headless && mod.modInfo.clientOnly)) {addModToContainer(mod); continue;}
            mod.enabled = true;
            try {
                addURL(modFile.toURI().toURL());
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            try {
                if (!Vars.headless) {
                    mod.icon = Draw.getBlankRegion();
                    URI resURI = new URI("jar:" + fileHandle.file().toURI().toString());
                    FileSystem fileSystem = null;
                    Path path;
                    if (resURI.getScheme().equals("jar")) {
                        fileSystem = FileSystems.newFileSystem(resURI, Collections.emptyMap());
                        path = fileSystem.getPath("resources");
                    } else {
                        path = Paths.get(resURI);
                    }

                    if(Files.exists(path)) {
                        Stream<Path> walk = Files.walk(path);
                        ArrayList<Path> paths = new ArrayList<>();
                        for (Iterator<Path> it = walk.iterator(); it.hasNext(); ) {
                            Path p = it.next();
                            paths.add(p);
                        }

                        if (fileSystem != null) {
                            SpriteLoader.loadSprites(mod, paths);
                            fileSystem.close();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(mod.modInfo.dependencies != null) {
                for (String dep : mod.modInfo.dependencies) {
                    if (dependencies.contains(dep, false)) continue;
                    dependencies.add(dep);
                }
            }

            modContainer.addMod(mod);
        }

        if (dependencies.size >= 0) {
            Array<String> missingDeps = new Array<>();
            Array<String> modUUIDs = getModUUIDs();

            for (String dep : dependencies) {
                if (!modUUIDs.contains(dep, false)) {
                    missingDeps.add(dep);
                }
            }

            if (missingDeps.size > 1) Logger.err("YOU ARE MISSING DEPENDENCIES");
            else if (missingDeps.size == 1) Logger.err("YOU ARE MISSING A DEPENDENCY");
            if (missingDeps.size >= 1) {
                for (String s : missingDeps) {
                    Logger.warn("- {0}", Base64Coder.decodeString(s));
                }

                Logger.warn("Not loading mods.");
                return;
            }
        }

        Reflections reflections = new Reflections(this.getClass().getClassLoader());
        modClasses = reflections.getTypesAnnotatedWith(Annotations.Mod.class);

        for (Class<?> cl : modClasses) {
            try {
                Method preInit = null;
                for (Method m : cl.getDeclaredMethods()) {
                    if (!m.isAnnotationPresent(Annotations.MMLEvent.class)) continue;
                    if (m.getAnnotation(Annotations.MMLEvent.class).event() == Annotations.Event.preinit) {
                        preInit = m;
                        break;
                    }
                }

                if (preInit == null) continue;

                Constructor<?> constructor = cl.getConstructor(); //One has to pass arguments if constructor takes input arguments.
                Object instance = constructor.newInstance();
                try {
                    preInit.invoke(instance);
                } catch (Exception e) {
                    Logger.err("MOD [mod name missing] FAILED TO PREINIT, THIS MIGHT CAUSE ISSUES, REPORT THIS TO THE MOD DEVELOPER");
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void modInit() {
        for (Class<?> mod : modClasses) {
            Method init = null;
            for (Method m : mod.getDeclaredMethods()) {
                if (!m.isAnnotationPresent(Annotations.MMLEvent.class)) continue;
                if (m.getAnnotation(Annotations.MMLEvent.class).event() == Annotations.Event.init) {
                    init = m;
                    break;
                }
            }

            if(init == null) continue;

            try {
                Constructor<?> ctor = mod.getConstructor(); //One has to pass arguments if constructor takes input arguments.
                Object instance = ctor.newInstance();
                init.invoke(instance);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void modPostInit() {
        for (Class<?> mod : modClasses) {
            Method postInit = null;
            for (Method m : mod.getDeclaredMethods()) {
                if (!m.isAnnotationPresent(Annotations.MMLEvent.class)) continue;
                if (m.getAnnotation(Annotations.MMLEvent.class).event() == Annotations.Event.postinit) {
                    postInit = m;
                    break;
                }
            }

            if(postInit == null)
                continue;

            try {
                Constructor<?> ctor = mod.getConstructor(); //One has to pass arguments if constructor takes input arguments.
                Object instance = ctor.newInstance();
                postInit.invoke(instance);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Mod getMod(String uuid) {
        for (Mod m : modContainer.getMods()) {
            if(m.uuid.equals(uuid)) return m;
        }

        return null;
    }

    public boolean addModToContainer(Mod mod) {
        return modContainer.addMod(mod);
    }

    public boolean isModEnabled(String uuid) {
        return enabledMods.contains(uuid, false);
    }

    public boolean enableMod(String uuid) {
        if(doesModExist(uuid)) return false;
        if(!enabledMods.contains(uuid, false)) {
            enabledMods.add(uuid);
            return true;
        }
        return false;
    }

    public boolean disableMod(String uuid) {
        if(doesModExist(uuid)) return false;
        if(enabledMods.contains(uuid, false)) {
            enabledMods.removeValue(uuid, false);
            return true;
        }
        return false;
    }

    public boolean doesModExist(String uuid) {
        for (Mod m:getMods()) {
            if(m.uuid.equals(uuid)) return true;
        }
        return false;
    }

    public Array<Mod> getMods(){
        return modContainer.getMods();
    }

    public Array<Mod> getEnabledMods(){
        Array<Mod> mods = new Array<>();
        for (Mod m:getMods()) {
            if(m.enabled)
                mods.add(m);
        }
        return mods;
    }

    public Array<String> getModUUIDs(){ return new Array<>(modContainer.getModUUIDs()); }

    public Array<String> getEnabledModUUIDs(){
        Array<String> uuids = new Array<>();
        for (Mod m:getMods()) {
            if(m.enabled)
                uuids.add(m.uuid);
        }
        return uuids;
    }

    public void delmod(Mod mod){
        modContainer.delMod(mod);
    }
}
