package io.anuke.mindustry.modloader.api;

import com.badlogic.gdx.files.FileHandle;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import io.anuke.mindustry.Vars;

import javax.annotation.CheckForNull;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Map;

public class Config {
    public static boolean configExist(String configName) {
        return Vars.modConfigDirectory.child(configName + ".yml").exists();
    }

    public static boolean writeConfig(String configName, Map map) {
        FileHandle configFile = Vars.modConfigDirectory.child(configName + ".yml");
        if(!configFile.exists()) createConfig(configName);
        try {
            YamlWriter writer = new YamlWriter(new FileWriter(configFile.path()));
            writer.write(map);
            writer.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean createConfig(String configName) {
        FileHandle configFile = Vars.modConfigDirectory.child(configName + ".yml");
        if (configFile.exists()) return false;
        try {if(configFile.file().createNewFile()) return true;} catch (Exception e) {e.printStackTrace(); return false;}
        return false;
    }

    @CheckForNull
    public static Map getConfig(String configName) {
        FileHandle configFile = Vars.modConfigDirectory.child(configName + ".yml");
        if(!configFile.exists()) return null;
        if (configFile.exists()) {
            try {
                YamlReader reader = new YamlReader(new FileReader(configFile.path()));
                return (Map) reader.read();
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    public static boolean writeIntoConfig(String configName, Object object) {
        FileHandle configFile = Vars.modConfigDirectory.child(configName + ".yml");
        if(!configFile.exists()) createConfig(configName);
        try {
            YamlWriter writer = new YamlWriter(new FileWriter(configFile.path()));
            writer.write(object);
            writer.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
