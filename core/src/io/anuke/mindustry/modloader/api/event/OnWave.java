package io.anuke.mindustry.modloader.api.event;

import io.anuke.mindustry.modloader.api.Event;

public class OnWave extends Event {

    public int wave;

    public OnWave(int wave) {
        super("onEvent");
        this.wave = wave;
    }

}
