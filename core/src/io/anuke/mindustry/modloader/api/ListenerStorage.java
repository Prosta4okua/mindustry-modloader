package io.anuke.mindustry.modloader.api;

import io.anuke.mindustry.modloader.api.Event;
import java.util.function.Consumer;

// we need this because otherwise java will complain about local variables
// used before they are initialized
// so this is a class that does nothing but store a value that may or may not
// acquire a value later on
public class ListenerStorage {
  public Consumer<Event> storedListener = null;
}
