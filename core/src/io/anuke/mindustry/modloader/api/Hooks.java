package io.anuke.mindustry.modloader.api;

import io.anuke.ucore.function.Consumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Hooks {
    //Render hooks
    public enum RenderHooks {
        floor, block, blockOverlay, item, teams, bullets, effects, end
    }

    private static HashMap<RenderHooks, List<Consumer>> renderHookMap = new HashMap<>();

    public static void initializeRenderHookMap() {
        for (RenderHooks rh: RenderHooks.values()) renderHookMap.put(rh, new ArrayList<>());
    }

    public static void onRender(RenderHooks hook, Consumer consumer) {
        List<Consumer> conList = renderHookMap.get(hook);
        conList.add(consumer);
        renderHookMap.put(hook, conList);
    }

    public static void rendererCall(RenderHooks hook) {
        for (Consumer c : renderHookMap.get(hook)) c.accept(null);
    }

    public static HashMap<RenderHooks, List<Consumer>> getRenderHookMap() {
        return renderHookMap;
    }
}
