package io.anuke.mindustry.modloader.api;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.function.Consumer;

public class EventEmitter {
  private Map<String, Vector<Consumer<Event>>> events;

  public EventEmitter() {
    events = new HashMap<>();
  }

  public void on(String name, Consumer<Event> listener) {
    Vector<Consumer<Event>> prev = events.getOrDefault(name, new Vector<>());
    prev.add(listener);
    events.put(name, prev);
  }

  public void once(String name, Consumer<Event> listener) {
    // this is here because a normal variable can't work here
    // see explanation in the definition of the class
    ListenerStorage storage = new ListenerStorage();
    Consumer<Event> newListener = ev -> {
      listener.accept(ev);
      this.removeListener(name, storage.storedListener);
    };
    storage.storedListener = newListener;
  }

  public boolean removeListener(String name, Consumer<Event> listener) {
    Vector<Consumer<Event>> listeners = events.get(name);
    if (listeners == null) return false;
    return listeners.remove(listener);
  }

  public void emit(Event event) {
    Vector<Consumer<Event>> toRun = events.get(event.name);
    if (toRun == null) return;
    for (Consumer<Event> listener : toRun) {
      listener.accept(event);
    }
  }
}
