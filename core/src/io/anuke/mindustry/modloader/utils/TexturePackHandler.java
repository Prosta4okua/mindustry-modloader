package io.anuke.mindustry.modloader.utils;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.modloader.objects.TexturePack;
import io.anuke.mindustry.modloader.objects.TexturePackContainer;
import io.anuke.mindustry.modloader.objects.TexturePackInfo;
import io.anuke.ucore.core.Core;
import io.anuke.ucore.core.Settings;

import java.io.*;
import java.nio.file.*;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class TexturePackHandler {

    public static TexturePackContainer container = new TexturePackContainer();

    public static void loadAllTexturePacks(){
        container.flipContainer();
        for (TexturePack tx : container.getTexturePacks()) {
            //System.out.println("attempting load " + tx.info.name);
            if(tx.enabled) {
                System.out.println("Loading");
                loadTexturePack(tx);
            }
        }
    }

    public static void removeTexturePack(TexturePack texturePack){
        container.removeTexturePack(texturePack);
    }

    public static void registerTexturePacks() {
        Array<String> order = Settings.getJson("orderTexturepacks", Array.class);
        Array<String> enabled = Settings.getJson("enabledTexturepacks", Array.class);

        container = new TexturePackContainer();
        TexturePackContainer presort = new TexturePackContainer();

        for(FileHandle file : Vars.texturePackDirectory.list()) {
            TexturePack tp = new TexturePack();
            tp.file = file;
            tp.info = loadMeta(file);
            if(tp.info == null) {
                Logger.warn("Failed loading texturepack file " + file.name() + "!");
                continue;
            }
            tp.enabled = enabled.contains(tp.info.name, false);
            presort.addTexturePack(tp);
        }

        for (String en : enabled){
            for(TexturePack tx : presort.getTexturePacks()){
                if(!tx.info.name.equals(en))
                    continue;
                container.addTexturePack(tx);
                presort.removeTexturePack(tx);
            }
        }

        System.out.println(presort.getTexturePacks().size);

        for(TexturePack tx : presort.getTexturePacks()) {
            container.addTexturePack(tx);
            presort.removeTexturePack(tx);
        }
    }

    public static void save(){
        Array<String> enabled = new Array<String>();
        Array<String> order = new Array<String>();

        for (TexturePack tx : TexturePackHandler.container.getTexturePacks()) {
            if (tx.enabled)
                enabled.add(tx.info.name);
            order.add(tx.info.name);
        }
        Settings.putJson("orderTexturepacks", order);
        Settings.putJson("enabledTexturepacks", enabled);
        Settings.save();
    }

    private static TexturePackInfo loadMeta(FileHandle meta){
        YamlReader yr = null;
        if(meta.extension().equals("zip")){
            try {
                ZipFile zipFile = new ZipFile(meta.file());
                InputStream input = zipFile.getInputStream(zipFile.getEntry("texturepackmeta"));
                yr = new YamlReader(new InputStreamReader(input));
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }else if (meta.isDirectory()) {
            meta = meta.child("texturepackmeta");
            if (meta.exists())
                yr = new YamlReader(meta.reader());
        } else
            return null;
        try {
            return yr.read(TexturePackInfo.class);
        } catch (YamlException e) {
            Logger.warn("Yaml Exception.");
            e.printStackTrace();
            return null;
        }
    }

    public static boolean loadTexturePack(TexturePack texturePack) {

        FileHandle file = texturePack.file;

        if(file.extension().equals("zip")) {
            try {
                ZipFile zipFile = new ZipFile(file.file());

                Enumeration<? extends ZipEntry> entries = zipFile.entries();

                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    if (entry.getName().endsWith(".png")) {

                        InputStream input = zipFile.getInputStream(entry);
                        String[] sray = entry.getName().replace(".png", "").split("/");
                        System.out.println(Arrays.toString(sray));
                        if (sray[0].equals("mindustry")) {
                            Pixmap p = new Pixmap(new Gdx2DPixmap(input, 0));
                            Texture t = new Texture(p);
                            TextureData td = t.getTextureData();
                            Core.atlas.addRegion(sray[1], t, 0, 0, td.getWidth(), td.getHeight());
                        } else {
                            Pixmap p = new Pixmap(new Gdx2DPixmap(input, 0));
                            Texture t = new Texture(p);
                            TextureData td = t.getTextureData();
                            Core.atlas.addRegion(sray[0] + ":" + sray[1], t, 0, 0, td.getWidth(), td.getHeight());
                        }
                    }
                }
            } catch (Exception ignored) {ignored.printStackTrace();}
        } else if (file.isDirectory()){
            try {
                Files.walk(file.file().toPath())
                        .parallel()
                        .filter(Files::isRegularFile)
                        .forEach(TexturePackHandler::replaceTexture);
            } catch (IOException e) {
                Logger.warn("IO Exception.");
                e.printStackTrace();
                return false;
            }

        } else {
            Logger.warn("Texture packs that are not folders or zip files wont be loaded.");
            return false;
        }

        return true;
    }

    private static void replaceTexture(Path path) {
        if(!path.toString().endsWith(".png")) return;
        try {
            FileHandle f = new FileHandle(path.toFile());
            Pixmap p = new Pixmap(f);
            Texture t = new Texture(p);
            TextureData td = t.getTextureData();
        }catch(GdxRuntimeException ex){
            Logger.warn("Couldn't load file: {0}", path.toString());
        }


        //Vars.modloader.atlas.addRegion(mod.modInfo.id + ":" + f.nameWithoutExtension(), t, 0,0, td.getWidth(), td.getHeight());
    }

    private static byte[] zipEntryToByteArray(InputStream is) throws IOException{
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = is.read(buffer)) > 0) {
            out.write(buffer, 0, len);
        }

        return out.toByteArray();
    }
}
