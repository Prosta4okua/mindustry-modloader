package io.anuke.mindustry.modloader.utils;

public class Colour {
    public static int[] hslToRgb(float h, float s, float l){
        float r, g, b;

        if(s == 0){
            r = g = b = l; // achromatic
        }else{
            float q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            float p = 2 * l - q;
            r = hue2rgb(p, q, h + 1/3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1/3);
        }

        return new int[] {Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)};
    }

    public float[] rgbToHsl(int r, int g, int b){
        if(r!=0) r /= 255;
        if(g!=0) g /= 255;
        if(b!=0) b /= 255;

        float max = Mathm.max(r, g, b);
        float min = Mathm.min(r, g, b);
        float l = (max + min) / 2;
        float h = l;
        float s;

        if(max == min){
            h = s = 0; // achromatic
        }else{
            float d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

            if(r == max) {
                    h = (g - b) / d + (g < b ? 6 : 0);
            } else if (g == max) {
                    h = (b - r) / d + 2;
            } else if (b == max) {
                    h = (r - g) / d + 4;
            }

            h /= 6;
        }

        return new float[] {h, s, l};
    }

    public static float hue2rgb(float p, float q, float t){
        if(t < 0) t += 1;
        if(t > 1) t -= 1;
        if(t < 1/6) return p + (q - p) * 6 * t;
        if(t < 1/2) return q;
        if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
        return p;
    }
}
