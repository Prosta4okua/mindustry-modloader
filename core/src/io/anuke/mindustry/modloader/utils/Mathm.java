package io.anuke.mindustry.modloader.utils;

public class Mathm {
    public static double max(double... n) {
        int i = 0;
        double max = n[i];

        while (++i < n.length)
            if (n[i] > max)
                max = n[i];

        return max;
    }

    public static int max(int... n) {
        int i = 0;
        int max = n[i];

        while (++i < n.length)
            if (n[i] > max)
                max = n[i];

        return max;
    }

    public static float max(float... n) {
        int i = 0;
        float max = n[i];

        while (++i < n.length)
            if (n[i] > max)
                max = n[i];

        return max;
    }

    public static double min(double... n) {
        int i = 0;
        double min = n[i];

        while (++i < n.length)
            if (n[i] < min)
                min = n[i];

        return min;
    }

    public static int min(int... n) {
        int i = 0;
        int min = n[i];

        while (++i < n.length)
            if (n[i] < min)
                min = n[i];

        return min;
    }

    public static float min(float... n) {
        int i = 0;
        float min = n[i];

        while (++i < n.length)
            if (n[i] < min)
                min = n[i];

        return min;
    }
}
