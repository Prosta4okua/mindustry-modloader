package io.anuke.mindustry.modloader.utils;

import io.anuke.ucore.util.ColorCodes;
import io.anuke.ucore.util.Strings;

/** Logger is an extended version of io.anuke.ucore.util.Log
 * (Currently only warn is new, more is on the TO-DO)
 */
public class Logger {
    private static boolean useColors = true;
    private static boolean disabled = false;
    private static LogHandler logger = new LogHandler();

    public static void setLogger(LogHandler log) {
        logger = log;
    }

    public static void setDisabled(boolean disabled) {
        Logger.disabled = disabled;
    }

    public static void setUseColors(boolean colors) {
        useColors = colors;
    }

    public static void info(String text, Object... args) {
        if (disabled) return;
        logger.info(text, args);
    }

    public static void info(Object object) {
        if (disabled) return;
        logger.info(String.valueOf(object));
    }

    public static void warn(String text, Object... args) {
        if (disabled) return;
        logger.warn(text, args);
    }

    public static void warn(Object object) {
        if (disabled) return;
        logger.warn(String.valueOf(object));
    }

    public static void err(String text, Object... args) {
        if (disabled) return;
        logger.err(text, args);
    }

    public static void err(Throwable th) {
        if (disabled) return;
        logger.err(th);
    }

    public static void print(String text, Object... args) {
        if (disabled) return;
        logger.print(text, args);
    }

    public static String format(String text, Object... args) {
        text = Strings.formatArgs(text, args);

        if (useColors) {
            for (String color : ColorCodes.getColorCodes()) {
                text = text.replace("&" + color, ColorCodes.getColorText(color));
            }
        } else {
            for (String color : ColorCodes.getColorCodes()) {
                text = text.replace("&" + color, "");
            }
        }
        return text;
    }

    public static class LogHandler {

        public void info(String text, Object... args) {
            print("&lg&fb" + format(text, args));
        }

        public void err(String text, Object... args) {
            print("&lr&fb" + format(text, args));
        }

        public void err(Throwable e) {
            if (useColors) System.out.print(ColorCodes.LIGHT_RED + ColorCodes.BOLD);
            e.printStackTrace();
            if (useColors) System.out.print(ColorCodes.RESET);
        }

        public void warn(String text, Object... args) {
            print("&ly&fb" + format(text, args));
        }

        void print(String text, Object... args){
            System.out.println(format(text + "&fr", args));
        }
    }
}
