package io.anuke.mindustry.modloader.objects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import io.anuke.ucore.graphics.Draw;

public class TexturePack {
    public TexturePack() {}

    public TexturePackInfo info;
    public boolean enabled;

    public TextureRegion icon = Draw.getBlankRegion();
    public FileHandle file;
}
