package io.anuke.mindustry.modloader.objects;

public class TexturePackInfo {
    public TexturePackInfo() {}

    public String name;
    public String author;
    public String desc;
    public String version;
}
