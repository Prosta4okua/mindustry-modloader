package com.examplemod.block;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.world.Tile;
import io.anuke.mindustry.world.blocks.defense.Wall;
import io.anuke.ucore.graphics.Draw;

public class ExampleWall extends Wall {

    ExampleWall(String name) {
        super(name);
    }
}
