package com.examplemod.content;

import com.badlogic.gdx.graphics.Color;
import com.examplemod.weapon.ModWeapons;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.type.Mech;
import io.anuke.ucore.core.Core;

public class ModMechs {
    public static Mech exampleMech;

    public static void onInitialization() {
        exampleMech = new Mech("examplemod:error", false) {
            {
                drillPower = 4;
                speed = 0.63f;
                boostSpeed = 0.86f;
                itemCapacity = 50;
                armor = 30f;
                weaponOffsetX = -1;
                weaponOffsetY = -1;
                weapon = ModWeapons.exampleGun;
                boostSpeed = 10f;
                maxSpeed = 14f;
                trailColor = Color.valueOf("d3ddff");
            }

            @Override
            public void load() {
                if (!flying) {
                    legRegion = Core.atlas.getRegion(name);
                    baseRegion = Core.atlas.getRegion(name);
                }

                region = Core.atlas.getRegion(name);
                iconRegion = Core.atlas.getRegion(name);
            }
        };
    }
}
