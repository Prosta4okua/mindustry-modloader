package com.examplemod.weapon;

import io.anuke.mindustry.content.fx.ShootFx;
import io.anuke.mindustry.type.Weapon;
import io.anuke.ucore.core.Core;

public class ModWeapons {
    public static Weapon exampleGun;

    public static void onInitialization() {
        exampleGun = new Weapon("examplemod:examplegun") {{
            length = 1f;
            reload = 0.5f;
            roundrobin = true;
            shots = 600;
            inaccuracy = 150f;
            recoil = 60f;
            velocityRnd = 70f;
            ejectEffect = ShootFx.shellEjectSmall;
        }

            @Override
            public void load() {
                region = Core.atlas.getRegion("examplemod:error");
                equipRegion = Core.atlas.getRegion("examplemod:error");
            }
        };
    }
}
